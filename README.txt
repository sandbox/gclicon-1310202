Allows ApacheSolr to index Media files attached to nodes as separate entities.

This module works by

Installation
------------
Add the module to your site modules folder and enable via the Modules admin screen.

Dependencies
------------
-- Media 1.x
-- ApacheSolr

This module will also integration with the media_translation module to store file language information in the solr document.


Requirements
------------
Your site configuration must meet the following requirements in order for media files to be indexed by solr:
-- All media file types must have a title field attached to them.  This should be the same title field on all media file types.
-- One or more content types that are indexed by solr must have a media selector field assigned to them.


Configuration
-------------

Configuring File Types:
-- Go to Configuration -> File Types (admin/config/media/file-types)
-- Click on Managed Fields for one of the file types
-- Create a generic File Title field of type "Text" or add an existing File Title field
   You must have a single Title field that is used by all File Types otherwise your
   files will not have a title in the search results.

-- Add any additional fields needed.
-- Repeat for all File Types

Configure Content Types:
-- Go to Structure -> Content types (admin/structure/types)
-- Click on manage fields
-- Add one or more "Multimedia asset" fields to the type

NOTE: You must have at least one content type that is indexed by Solr containing a Multimedia asset field.
It is recommended to re-use the same field across multiple content types if possible.

Configure Solr Integration
-- Go to Configuration -> Apache Solr Search (admin/config/search/apachesolr)
-- Click on the "Media" tab
-- Select the field to use as the media file title.
-- Select the media fields attached to nodes to include in the solr index.

Rebuild the solr index
-- Go to Configuration -> Apache Solr Search (admin/config/search/apachesolr)
-- Click on the "Search Index" tab
-- Select either "Queue content for reindexing" or "Delete the index"
