<?php

/**
 * @file
 * Administration pages for the module.
 */

/**
 * Call back for the menu hook to display the
 * administration tab.
 */
function apachesolr_media_admin_page() {
  $form = array();
  $fields = field_info_fields();
  $title_field_options = array();
  $media_field_options = array();
  foreach ($fields as $field_name => $field) {
    if (isset($field['bundles']['file'])) {
      $title_field_options[$field_name] = $field_name;
    }
    if ($field['type'] == 'media' && isset($field['bundles']['node'])) {
      $media_field_options[$field_name] = $field_name;
    }
  }
  ksort($title_field_options);
  ksort($media_field_options);

  $form['apachesolr_media_title_field'] = array(
    '#type' => 'select',
    '#title' => t('File Title Field'),
    '#description' => t('Select the field on files that contains the Title to display.'),
    '#options' => $title_field_options,
    '#default_value' => variable_get('apachesolr_media_title_field', ''),
  );

  $form['apachesolr_media_file_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Media fields to index'),
    '#description' => t('Select the media fields on nodes that contain the files to include in the index.'),
    '#options' => $media_field_options,
    '#default_value' => variable_get('apachesolr_media_file_fields', ''),
  );

  return system_settings_form($form);
}